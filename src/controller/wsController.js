const jwt = require('jsonwebtoken')
const query = require('../service/db')
const { SECRET } = require('../config/config')

// 获取当前账号相关数据
async function curAccount(authorization) {
    if (!authorization) return
    let token = authorization.split(' ')[1]
    return await jwt.verify(token, SECRET)
}


// 连接池
let connectWs = {}

// 接口方法
module.exports = {

    // 初始化webSocket
    newWebSocket(app) {

        // websocket连接
        app.ws('/ws/:id', (ws, req) => {
            // 初始化
            const { id } = req.params
            connectWs[id] = []

            // 添加至连接池
            connectWs[id].push(ws)

            // 关闭之后清空连接池
            ws.on('close', () => {
                // connectWs[id] = []
            })

            // 连接不存在就删除key
            if (connectWs[id].length <= 0) delete connectWs[id]
        })


        // 获取消息
        app.get('/api/message/list', async (req, res) => {

            // 参数处理
            let { id } = req.query
            if (!id) return res.send({ code: 10001, msg: '请求失败' })

            //当前登录账号相关数据
            const { account_id } = await curAccount(req.headers.authorization)

            // 查询该账号是否存在
            const aSql = `select t2.id,t2.nickname,t2.state,t2.pic from user_relation t1 
                        left join user t2 on t1.user_id=t2.id 
                        where t1.user_id=?`
            const accountData = await query(aSql, [id])
            if (accountData.length <= 0) return res.send({ code: 10001, msg: '查询不到好友关系' })
            if (id == account_id) return res.send({ code: 10001, msg: '目前暂不支持自己与自己对话' })


            // 查询会话窗口数据
            const to_key = id + '-' + account_id
            const from_key = account_id + '-' + id
            const cSql = `select * from chat_relation where to_key=? or from_key=?`
            const chatData = await query(cSql, [to_key, from_key])

            // 会话窗口数据是否存在，不存在就创建会话窗口
            if (chatData.length <= 0) {
                const iSql = `insert into chat_relation(message_to,message_from,create_time,to_key,from_key) values(?,?,?,?,?)`
                const newChat = await query(iSql, [id, account_id, new Date().getTime(), to_key, from_key])
            }

            // 查询会话窗口头像
            const pSql = `select t1.*,t2.pic as friend_pic,t2.nickname as friend_nickname,
                        t3.pic as sender_pic,t3.nickname as sender_nickname from user_relation t1 
                        left join user t2 on t1.user_relation_id=t2.id 
                        left join user t3 on t1.user_id=t3.id 
                        where t1.user_id=? and t1.user_relation_id=?;`
            const portraitPic = await query(pSql, [account_id, id])

            // 数据库查询
            const sSql = `select * from message where chat_key=? or chat_key=?`
            query(sSql, [to_key, from_key]).then(result => {

                // 账号备注名称
                let info = portraitPic[0]
                let userData = {}
                for (let key in info) {
                    if (key != 'remarks') {
                        userData[key] = info[key]
                    }
                }
                if (info.remarks && info.remarks.length > 0) {
                    userData.friend_nickname = info.remarks
                }
                res.send({ code: 10002, msg: '请求成功', data: { user: userData, list: result } })
            }).catch(err => {
                res.send({ code: 10005, msg: err.message })
            })
        })

        // 推送消息
        app.post('/api/message', async (req, res) => {

            // 参数处理
            let { message_to, message_from, message, type, file_path } = req.body
            if (!message_to || !message_from || !message) return res.send({ code: 10001, msg: '请求失败' })


            //当前登录账号相关数据
            const { account_id, nickName: accountNickName } = await curAccount(req.headers.authorization)

            // 查询该账号是否存在
            const aSql = `select t1.* from user_relation t1 
                        left join user t2 on t1.user_relation_id=t2.id 
                        where t1.user_id=? and t1.user_relation_id=?`
            const accountData = await query(aSql, [account_id, message_to])
            if (accountData.length <= 0) return res.send({ code: 10001, msg: '查询不到好友关系' })
            if (message_to == account_id) return res.send({ code: 10001, msg: '目前暂不支持自己与自己对话' })


            // 查询账号备注信息
            const rSql = `select t1.*,t2.pic,t2.nickname from user_relation t1 
                        left join user t2 on t1.user_relation_id=t2.id 
                        where t1.user_id=? and t1.user_relation_id=?`
            const remarksData = await query(rSql, [message_to, account_id])
            let { pic, remarks, nickname } = remarksData[0]
            let remarksNickname = remarks ? remarks : nickname


            // 存储数据库
            const create_time = new Date().getTime()
            const chatKey = account_id + '-' + message_to
            const iSql = `insert into message (message_to,message_from,sender_nickname,message,chat_key,type,file_path,create_time) 
                        values(?,?,?,?,?,?,?,?)`
            query(iSql, [message_to, message_from, accountNickName, message, chatKey, type, file_path, create_time]).then(result => {
                // 连接是否存在，存在就立即推送消息
                if (connectWs[message_to]) {
                    let WsParams = { message_to, message_from, message, nickname: remarksNickname, create_time, type, file_path, pic }
                    connectWs[message_to].forEach(item => {
                        item.send(JSON.stringify(WsParams))
                    })
                }
                res.send({ code: 10002, msg: '发送成功' })
            }).catch(err => {
                res.send({ code: 10005, msg: err })
            })
        })
    },
    connectWs
}