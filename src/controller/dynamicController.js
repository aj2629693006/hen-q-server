const query = require('../service/db')
const initTree = require('../utils/tree')
const { connectWs } = require('./wsController')

module.exports = {
    // 发布动态
    async addDynamic(req, res) {
        let { content, filePath } = req.body
        if (!content) return res.send({ code: 10001, msg: '参数异常' })

        //当前登录账号相关数据
        let { account_id } = req.auth

        let time = new Date().getTime()
        let data = [content, time, account_id]

        if (filePath) {
            data.push(filePath)
        }

        // 存储
        const iSql = `insert into dynamic(content,create_time,user_id${filePath ? ',filePath' : ''}) values(?,?,?${filePath ? ',?' : ''})`
        query(iSql, data).then(result => {
            res.send({ code: 10002, msg: '发布成功' })
        }).catch(err => {
            res.send({ code: 10005, msg: err })
        })

    },

    // 动态列表
    async dynamicList(req, res) {

        let { pagenum, pagesize } = req.query
        let size = parseInt(pagesize) || 10
        let num = parseInt(pagenum) || 1
        let sum = (num - 1) * size

        //当前登录账号相关数据
        let { account_id } = req.auth

        const sSql = `select t2.nickname,t2.pic,t3.* from user_relation t1 left join user t2 
                      on t1.user_relation_id=t2.id left join dynamic t3 on t2.id=t3.user_id where 
                      t1.user_id=? and t3.content is not null order by create_time desc limit ?,?`
        query(sSql, [account_id, sum, size]).then(async (result) => {

            // 动态总数
            const tSql = `select count(*) as total from (select t2.nickname,t2.pic,t3.* from user_relation t1
                          left join user t2 on t1.user_relation_id=t2.id left join dynamic t3 on t2.id=t3.user_id 
                          where t1.user_id=? and t3.content is not null) t4`
            const total = await query(tSql, [account_id])
            res.send({
                code: 10002, msg: '请求成功',
                data: {
                    list: result,
                    total: total[0].total
                }
            })

        }).catch(err => {
            res.send({ code: 10005, msg: err })
        })
    },

    // 获取单条数据
    async dynamicOne(req, res) {
        let { id, type, pagenum, pagesize } = req.query
        if (!id) return res.send({ code: 10001, msg: '参数异常' })
        let size = parseInt(pagesize) || 10
        let num = parseInt(pagenum) || 1
        let sum = (num - 1) * size

        const mSql = `select t1.*,t2.pic,t2.nickname,t3.remarks from dynamic_message t1 left join user t2 
                    on t1.account_id=t2.id left join user_relation t3 on t3.user_relation_id=t2.id where 
                    t1.dynamic_id=? group by id order by create_time desc limit ?,?`
        // 获取评论总数
        const tSql = `select count(*) as total from (select t1.*,t2.pic,t2.nickname,t3.remarks from dynamic_message t1 
                    left join user t2 on t1.account_id=t2.id left join user_relation t3 on t3.user_relation_id=t2.id 
                    where t1.dynamic_id=? and t1.parent_id is null and t3.remarks is not null group by id) t4`

        const messageList = await query(mSql, [id, sum, size])
        const total = await query(tSql, [id])

        // 动态信息和评论列表进行区分
        if (type && type == 'message') {
            let data = {
                message: initTree(messageList),
                total: total[0].total
            }
            res.send({ code: 10002, msg: '请求成功', data })
        } else {
            const sSql = 'select t1.*,t2.pic,t2.nickname from dynamic t1 left join user t2 on t1.user_id=t2.id where t1.id=?'
            query(sSql, [id]).then(async (result) => {
                let data = {
                    dynamic_info: result,
                    message: initTree(messageList),
                    total: total[0].total
                }
                res.send({ code: 10002, msg: '请求成功', data })
            }).catch(err => res.send({ code: 10005, msg: err }))
        }

    },

    // 动态评论
    dynamicMessage(req, res) {
        // 参数处理
        let { content, id, account_id, parent_id, reply_id } = req.body
        if (!content || !id || !account_id) return res.send({ code: '10001', msg: '参数异常' })
        
        // 评论存储
        const iSql = `insert into dynamic_message (content, dynamic_id,account_id,
                      create_time${parent_id ? ',parent_id' : ''}${reply_id ? ',reply_id' : ''}) 
                      values(?,?,?,?${parent_id ? ',?' : ''}${reply_id ? ',?' : ''})`

        let data = [content, id, account_id, new Date().getTime()]
        if (parent_id) {
            data.push(parent_id)
        }
        if (reply_id) {
            data.push(reply_id)
        }

        query(iSql, data).then(async (result) => {
            const sSql = `select t1.*,t2.pic,t2.nickname,t3.nickname as reply_nickname,t3.pic as reply_pic,t4.content as 
                          dynamci_content from dynamic_message t1 left join user t2 on t1.account_id=t2.id left join user t3
                          on t1.reply_id=t3.id left join dynamic t4 on t1.dynamic_id=t4.id where t1.id=?`
            const msgData = await query(sSql, [result.insertId])

            //当前登录账号相关数据
            let { account_id } = req.auth

            /**
             * 1：回复存在回复者的ID
             * 2：并且回复者不能是自己
             * 3：并且存在websocket中
             * */
            if (reply_id && account_id != reply_id && connectWs[reply_id]) {
                connectWs[reply_id].forEach(item => {
                    let resultData = { ...msgData[0], type: 'dynamic' }
                    item.send(JSON.stringify(resultData))
                });
            }

            res.send({ code: 10002, msg: '评论成功', data: msgData[0] })

        }).catch(err => {
            console.log(err);
            res.send({ code: 10005, msg: err })
        })
    },

    // 获取更多评论
    dynamicMsgMore(req, res) {

        // 参数处理
        let { id, pagenum, pagesize } = req.query
        if (!id) return res.send({ code: 10001, msg: '参数异常' })
        let size = parseInt(pagesize) || 10
        let num = parseInt(pagenum) || 1
        let sum = (num - 1) * size
        
        // 分页查询
        const sSql = `select t1.*,t2.pic,t2.nickname,t3.nickname as reply_nickname,t3.pic as reply_pic  from dynamic_message t1 
                    left join user t2 on t1.account_id=t2.id left join user t3 on t1.reply_id=t3.id
                    where t1.parent_id=? order by create_time desc limit ?,?`
        query(sSql, [id, sum, size]).then(async (result) => {
            res.send({ code: 10002, data: result })
        }).catch(err => res.send({ code: 10005, msg: err }))
    },

    // 获取我的动态
    Mydynamic(req, res) {

        // 参数处理
        let { id, pagenum, pagesize } = req.query
        if (!id) return res.send({ code: 10001, msg: '参数异常' })
        let size = parseInt(pagesize) || 10
        let num = parseInt(pagenum) || 1
        let sum = (num - 1) * size

        // 分页查询
        const sSql = `select t1.*,t2.nickname,t2.pic from dynamic t1 left join user t2 on t1.user_id=t2.id 
                    where t1.user_id=? order by create_time desc limit ?,?`
        query(sSql, [id, sum, size]).then(async (result) => {

            // 查询我的动态总数
            const tSql = `select count(*) as total from dynamic t1 left join user t2 on t1.user_id=t2.id where t1.user_id=?`
            const total = await query(tSql, [id])
            res.send({ code: 10002, msg: '请求成功', data: { list: result, total: total[0].total } })

        }).catch(err => res.send({ code: 10005, msg: err }));

    }
}