const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const query = require('../service/db')
const { SECRET } = require('../config/config')
const { connectWs } = require('./wsController')
const { registerValidate, loginValidate } = require('../utils/validate')


// 记录账号登录
async function loginRecord(val) {
    // 是否存在记录中
    let sSql = `select * from login_log where login_id=?`
    let account = await query(sSql, [val.account_id])

    let sql;
    let data;
    if (account.length === 0) {
        // 添加记录
        sql = `insert into login_log(login_id,login_key,login_time) values(?,?,?)`
        data = [val.account_id, val.key, val.time]
    } else {
        // 修改记录
        sql = `update login_log set login_key=?,login_time=? where login_id=?`
        data = [val.key, val.time, val.account_id]
    }

    // sql
    query(sql, data)
}


module.exports = {

    // 登录
    async login(req, res) {

        // 参数处理
        let { state, message, email, password } = loginValidate(req.body)
        if (!state) return res.send({ code: 10001, msg: message })


        // 账号是否存在
        const sSql = 'select * from user where email=?'
        const accountData = await query(sSql, [email])
        if (accountData.length <= 0) return res.send({ code: 10006, msg: '账号不存在，请前往注册' })


        // 账号信息对比
        const bool = await bcrypt.compare(password + '_' + email, accountData[0].password)
        if (!bool) return res.send({ code: 10001, msg: '邮箱或密码错误' })


        // 生成token
        let time = new Date().getTime() + ''
        let login_key = time + `_${accountData[0].email}_login`
        let params = {
            nickName: accountData[0].nickname,
            account_id: accountData[0].id,
            email: accountData[0].email,
            key: login_key
        }
        const token = await jwt.sign(params, SECRET, { expiresIn: 1000 * 60 * 60 * 48 })

        // 异地登录推送
        if (connectWs[accountData[0].id]) {
            connectWs[accountData[0].id].forEach(item => {
                item.send(JSON.stringify({ code: 10008, msg: '你的账号已在其他设备登录，当前设备已下线' }))
            })
        }

        // 登录记录
        loginRecord({ ...params, time })

        res.send({ code: 10002, msg: '登录成功', token: 'Breaer ' + token })
    },

    // 退出登录
    async loginOut(req, res) {
        query(`delete from login_log where login_id=?`, [req.auth.account_id]).then(result => {
            res.send({ code: 10002, msg: '退出成功', })
        }).catch(err => {
            res.send({ code: 10005, msg: err })
        })
    },

    // 注册
    async register(req, res) {

        // 参数处理
        let { state, message, nickname, email, password } = registerValidate(req.body)
        if (!state) return res.send({ code: 10001, msg: message })


        // 邮箱查重
        const aSql = 'select nickname from user where email=?'
        const accountData = await query(aSql, [email])
        if (accountData.length > 0) return res.send({ code: 10007, msg: '该账号已存在' })


        // 密码加密
        let genSalt = await bcrypt.genSalt(10)
        let pwd = await bcrypt.hash(password + '_' + email, genSalt)

        // 存储数据库
        const sql = `insert into user(nickname,email,password,create_time,pic) values (?,?,?,?,?)`
        
        // 系统随机头像
        let picPath = `\\portrait\\${Math.floor(Math.random() * 8)}.jpeg`
        query(sql, [nickname, email, pwd, new Date().getTime(), picPath]).then(result => {

            // 好友关系表
            const iSql = `insert into user_relation(user_id,user_relation_id) values(?,?)`
            query(iSql, [result.insertId, result.insertId])

            res.send({ code: 10002, msg: '注册成功' })

        }).catch(err => {
            res.send({ code: 10005, msg: err })
        })

    },

    // 获取账号信息
    async accountInfo(req, res) {

        let { account_id } = req.auth

        // 账号数据查询
        const sSql = `select nickname,id,email,state,create_time,user_type,pic,sign,constellation from user where id=?`
        query(sSql, [account_id]).then((result) => {
            if (result.length > 0) {
                res.send({ code: 10002, msg: '请求成功', data: result[0] })
            } else {
                res.send({ code: 10002, msg: '该账号不存在' })
            }
        }).catch(err => {
            res.send({ code: 10005, msg: err })
        })
    },

    // 账号搜索
    async accountSearch(req, res) {

        // 参数处理
        let { nickname } = req.body
        if (!nickname) res.send({ code: 10001, msg: '参数异常' })


        //当前登录账号相关数据
        let { account_id } = req.auth


        // 查询账号信息
        const aSql = `select nickname,email,id,pic from user  where nickname=?`
        const accountData = await query(aSql, [nickname])
        if (accountData.length <= 0) return res.send({ code: 10006, msg: '该账号不存在' })


        // 查询是否有好友关系
        const sSql = `select * from user_relation where user_id=? and user_relation_id=?`
        query(sSql, [account_id, accountData[0].id]).then((result) => {

            let data = {
                ...accountData[0],
            }
            if (data.id == account_id || result.length > 0) {
                data.friend = true
            } else {
                data.friend = false
            }
            res.send({ code: 10002, msg: '请求成功', data: [data] })
        }).catch(err => {
            res.send({ code: 10005, msg: err })
        })

    },

    // 获取单个账号
    async accountOne(req, res) {

        // 参数处理
        let { id } = req.query
        if (!id) return res.send({ code: 10001, msg: '参数异常' })


        //当前登录账号相关数据
        let { account_id } = req.auth


        // 查询账号信息
        const aSql = `select nickname,email,id,pic,sign,constellation from user  where id=?`
        const accountData = await query(aSql, [id])
        if (accountData.length <= 0) return res.send({ code: 10006, msg: '该账号不存在' })


        // 查询是否有好友关系
        const sSql = `select * from user_relation where user_id=? and user_relation_id=?`
        query(sSql, [account_id, accountData[0].id]).then((result) => {
            let data = {
                ...accountData[0],
            }

            // 判断查询的账号信息是否跟当前登录账号好友关系
            if (result.length > 0) {
                data.friend = true
                data.remarks = result[0].remarks
            } else {
                data.friend = false
            }
            res.send({ code: 10002, msg: '请求成功', data: data })
        }).catch(err => {
            res.send({ code: 10005, msg: err })
        })

    },

    // 添加好友
    async addFriends(req, res) {

        // 参数处理
        let { id } = req.body
        if (!id) return res.send({ code: 10001, msg: '参数异常', })


        //当前登录账号相关数据
        let { account_id } = req.auth


        // 存储数据
        const iSql = `insert into user_relation(user_id,user_relation_id) values (?,?),(?,?)`
        query(iSql, [account_id, id, id, account_id]).then(result => {
            res.send({ code: 10002, msg: '添加成功', data: req.body })
        }).catch(err => {
            res.send({ code: 10005, msg: err })
        })
    },



    // 账号列表
    async accountList(req, res) {

        //当前登录账号相关数据
        let { account_id } = req.auth

        const cSql = `select count(*) as total from message where message_from = ? or message_to = ?`
        const msgCount = await query(cSql, [account_id, account_id])

        // 查询数据
        const sSql = `select t7.*,t8.remarks from (select * from (select t5.*,m1.message_from,m1.message_to,m1.message,m1.chat_key,
                      m1.type,m1.create_time,m1.read_state from (select * from (select t1.to_key,t1.from_key,t2.user_id,t3.id,t3.nickname,
                      t3.pic from chat_relation t1 left join user_relation t2 on (t1.message_from=t2.user_id or t1.message_to=t2.user_id) 
                      and not t2.user_id=? left join user t3 on t2.user_id=t3.id where t1.message_from=? or t1.message_to=?) t4 
                      group by user_id) t5 left join (select * from message order by create_time desc limit 10000000000) m1 on 
                      m1.chat_key=t5.to_key or m1.chat_key=t5.from_key) t6 group by user_id) t7 left join (select * from user_relation 
                      where user_id=?) t8 on t7.user_id=t8.user_relation_id order by create_time desc`
        query(sSql, [account_id, account_id, account_id, account_id]).then(result => {
            res.send({ code: 10002, msg: '请求成功', data: result })
        }).catch(err => {
            res.send({ code: 10005, msg: err })
        })
    },


    // 好友列表
    async friendList(req, res) {

        //当前登录账号相关数据
        let { account_id } = req.auth


        // 好友查询
        let sSql = `select t1.user_id,t1.user_relation_id,t1.remarks,t2.nickname,t2.email,t2.id,t2.state,t2.create_time,t2.pic
                    from user_relation t1 left join user t2 on t1.user_relation_id=t2.id where t1.user_id=?`
        query(sSql, [account_id]).then(result => {
            res.send({ code: 10002, msg: '请求成功', data: result })
        }).catch(err => {
            res.send({ code: 10005, msg: err })
        })
    },

    // 获取账号的配置信息
    async setting(req, res) {
        let { id, type } = req.query
        if (!id || !type) return res.send({ code: 10001, msg: '参数异常' })

        //当前登录账号相关数据
        let { account_id } = req.auth

        // SQL拼接
        let sSql = '';
        let data;
        if (type === 'account') {
            sSql = `select nickname,sign,constellation,id from user where id=?`
            data = [id]

        } else if (type === 'friend') {
            sSql = `select t1.remarks,t2.nickname,t2.id from user_relation t1 left join user t2 on t1.user_relation_id=t2.id where t1.user_id=? and t1.user_relation_id=?`
            data = [account_id, id]
        } else {
            return res.send({ code: 10001, msg: '参数异常' })
        }


        // 数据查询
        console.log(sSql);
        query(sSql, data).then((result) => {
            console.log(result);
            res.send({ code: 10002, msg: '请求成功', data: result[0] })
        }).catch(err => {
            res.send({ code: 10005, msg: err })
        })
    },

    // 修改账号相关数据
    async updateSettings(req, res) {

        // 参数处理
        let { id, nickname, sign, constellation, type } = req.body
        let types = ['account', 'friend']
        if (!type || !types.includes(type) || !id) return res.send({ code: 10001, msg: '参数异常' })

        // 全局判断
        if (!nickname || nickname.length < 2 || nickname.length > 9) {
            return res.send({ code: 10001, msg: '昵称长度:2~8个字符' })
        }

        // 当前登录账号判断
        if (type === 'account') {
            if (!constellation) {
                return res.send({ code: 10001, msg: '星座不能为空' })
            } else if (sign && (sign.length <= 0 || sign.length > 150)) {
                return res.send({ code: 10001, msg: '签名长度:1~150个字符' })
            }
        }


        //当前登录账号相关数据
        let { account_id } = req.auth

        let data = []
        let uSql = ''

        if (type === 'account') {
            // 当前账号
            uSql = `update user set nickname=?,sign=?,constellation=? where id=?`
            data = [nickname, sign, constellation, account_id]

        } else if (type === 'friend') {
            // 好友
            let sSql = `select * from user_relation where user_id=? and user_relation_id=?`
            let friendData = await query(sSql, [account_id, id])
            if (friendData.length <= 0) {
                return res.send({ code: 10001, msg: '非法操作' })
            }
            uSql = `update user_relation set remarks=? where user_id=? and user_relation_id=?`
            data = [nickname, account_id, id]
        }

        // 执行SQL
        query(uSql, data).then(result => {
            res.send({ code: 10002, msg: '修改成功' })
        }).catch(err => {
            res.send({ code: 10005, msg: err })
        })

    },

    // 删除好友
    async delFriend(req, res) {
        let { id } = req.params
        if (!id) return res.send({ code: 10001, msg: '参数异常' })

        //当前登录账号相关数据
        let { account_id } = req.auth

        let dSql = `delete from user_relation where user_id=? and user_relation_id=?`
        query(dSql, [account_id, id])
        query(dSql, [id, account_id])

        let cSql = `delete from chat_relation where message_from=? and message_to=?`
        query(cSql, [account_id, id])
        query(cSql, [id, account_id]).then(result => {
            res.send({ code: 10002, msg: '删除成功' })
        })
    },

    // 消息是否已读
    async messageRead(req, res) {
        // 参数处理
        let { id } = req.params
        if (!id) return res.send({ code: 10001, msg: '参数异常' })

        //当前登录账号相关数据
        let { account_id } = req.auth

        // 修改已读状态
        let uSql = `update message set read_state=? where message_to=? and message_from=?`
        query(uSql, [0, account_id, id]).then(result => {
            res.send({ code: 10002, msg: '请求成功' })

        }).catch(err => res.send({ code: 10005, msg: err }))
    }
}
