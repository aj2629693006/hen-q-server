module.exports = {
    
    // 数据库配置数据
    dataBase: {
        host: 'localhost',
        port: 3306,
        user: 'aj',
        password: '123456',
        database: 'chat'
    },

    // jwt加密密钥
    SECRET: 'ADMIN_._SECRET_._TOKEN_._JWT'
}