const mysql = require('mysql')

// 数据库相关参数
const { dataBase } = require('../config/config')

// 连接数据库
const db = mysql.createConnection({
    host: dataBase.host,
    port: dataBase.port,
    user: dataBase.user,
    password: dataBase.password,
    database: dataBase.database,
})

// 检测是否连接成功
db.connect(err => {
    if (err !== null) {
        console.log('数据库连接失败!');
    } else {
        console.log('数据库连接成功!');
    }
})

// 配置数据库查询方法
module.exports = function query(sql, data = []) {
    return new Promise((resolve, reject) => {
        try {
            if (!sql) return reject('sql禁止为空!')
            db.query(sql, data, (err, result) => {
                if (err) return reject(err.message)
                resolve(result)
            })
        } catch (err) {
            if (err) return reject(err.message)
        }
    })
}
