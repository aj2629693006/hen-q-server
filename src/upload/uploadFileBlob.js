const path = require('path')
const formidable = require('formidable')
const fs = require('fs')
const query = require('../service/db')

module.exports = (req, res) => {
    try {
        // formidable配置项
        const formidableOptions = {
            multiples: true,
            keepExtensions: true,
            maxFileSize: 5 * 1024 * 1024,
            uploadDir: path.join(__dirname, '../..', 'public/images'),

        }

        // 初始化
        const form = formidable(formidableOptions)

        // 控制上传文件大小
        form.on('progress', function (bytesReceived, bytesExpected) {
            const size = 1024 * 1024 * 5
            if (bytesExpected > size) {
                form.on('error', '上传文件大于:1MB,请按照文件大小上传!')
            }

        })

        // 处理文件信息
        form.parse(req, (err, fileds, files) => {

            let fileType = ['image/png', 'image/jpeg', 'image/jpg']
            if (!fileType.includes(files.file.mimetype)) {
                return res.send({ code: 10001, msg: '允许上传文件格式：image/png, image/jpeg, image/jpg' })
            }

            // 存在错误信息
            if (err) return res.status(500).send({ code: 10005, msg: '系统错误请联系管理员!' })

            // 文件数据处理
            const { file } = files

            // 由于是blob可是formidable没法识别后缀名所有直接读取文件之后就更改文件名
            fs.rename(file.filepath, file.filepath + '.png', async (FileErr) => {
                if (FileErr) {
                    res.status(500).send({ code: 10005, msg: '系统错误请联系管理员!' })
                } else {
                    const url = file.filepath.split('public')[1] + '.png'
                    let { account_id } = req.auth
                    const sql = `update user set pic=? where id=?`
                    query(sql, [url, account_id]).then(result => {
                        res.send({ code: 10002, msg: '上传成功' })

                    }).catch(sqlErr => {
                        res.send({ code: 10005, msg: sqlErr })
                    })
                }
            })
        })

    } catch (e) {
        if (e) res.status(500).send({ code: 10005, msg: e.message })
    }
}