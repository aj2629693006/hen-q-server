const path = require('path')
const formidable = require('formidable')
const fs = require('fs')

module.exports = function uploadFile(req, res) {
    try {
        // formidable配置项
        const formidableOptions = {
            multiples: true,
            keepExtensions: true,
            maxFileSize: 100 * 1024 * 1024,
            uploadDir: path.join(__dirname, '../..', 'public/file'),

        }

        // 初始化
        const form = formidable(formidableOptions)

        // 控制上传文件大小
        form.on('progress', function (bytesReceived, bytesExpected) {
            const size = 1024 * 1024 * 500
            if (bytesExpected > size) {
                form.on('error', '上传文件大于:100MB,请按照文件大小上传!')
            }
        })

        // 处理文件信息
        form.parse(req, (err, fileds, files) => {
            
            // 存在错误信息
            if (err) return res.status(500).send({ code: 10005, msg: '系统错误请联系管理员!' })

            // 文件数据处理
            const { file } = files
            const resultFiles = []

            if (file instanceof Array) {
                // 多个文件
                file.forEach((item) => {
                    const url = item.filepath.split('public')[1]
                    resultFiles.push({ url })
                })
            } else {
                // 单个文件
                const url = file.filepath.split('public')[1]
                resultFiles.push({ url })
            }
            let total = resultFiles.length > 1 ? resultFiles : resultFiles[0]
            res.send({ code: 200, msg: '上传成功', data: total })
        })

    } catch (e) {
        if (e) res.status(500).send({ code: 10005, msg: e.message })
    }
}