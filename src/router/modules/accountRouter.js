const express = require('express')
const router = express.Router()

const {
    accountInfo, accountList, accountSearch, accountOne, addFriends,
    friendList, setting, updateSettings, delFriend, messageRead,
    loginOut
} = require('../../controller/accountController')

// 账号信息
router.get('/info', accountInfo)

// 账号列表
router.get('/list', accountList)

// 账号搜索
router.post('/search', accountSearch)

// 单个账号
router.get('/One', accountOne)

// 添加好友
router.post('/addFriend', addFriends)

// 好友列表
router.get('/friends', friendList)

// 获取账号配置信息
router.get('/setting', setting)

// 修改相关账号数据
router.post('/setting/edit', updateSettings)

// 删除好友
router.post('/delFriend/:id', delFriend)

// 消息是否已读
router.put('/messageRead/:id', messageRead)

// 退出登录
router.post('/loginout', loginOut)

module.exports = router
