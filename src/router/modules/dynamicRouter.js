const express = require('express')
const router = express.Router()

const { addDynamic, dynamicList, dynamicOne,
    dynamicMessage,dynamicMsgMore,Mydynamic
} = require('../../controller/dynamicController')

// 发布
router.post('/add', addDynamic)

// 列表
router.get('/list', dynamicList)

// 获取单条数据
router.get('/one', dynamicOne)

// 评论
router.post('/message', dynamicMessage)

// 更多评论
router.get('/message/more', dynamicMsgMore)

// 我的动态
router.get('/my', Mydynamic)

module.exports = router