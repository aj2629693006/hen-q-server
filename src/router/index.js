const express = require('express')
const router = express.Router()

// 基础路由
const { login, register } = require('../controller/accountController')

// 登录 、 注册
router.post('/login', login)
router.post('/register', register)


// 文件上传
const uploadFile = require('../upload/uploadFile')
const uploadFileBlob = require('../upload/uploadFileBlob')
router.post('/upload', uploadFile)
router.post('/uploadBlob', uploadFileBlob)


// 账号模块
const accountRouter = require('./modules/accountRouter')
router.use('/account', accountRouter)

// 动态模块
const dynamicRouter = require('./modules/dynamicRouter')
router.use('/dynamic', dynamicRouter)

// 路由实例
module.exports = router
