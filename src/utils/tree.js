// 初始化树结构
function initTree(data) {
    let parent = []
    // 获取父级
    for (let item of data) {
        if (item.parent_id == null) {
            parent.push(item)
            initChild(item, data)
        }
    }
    parent.sort((a, b) => b.create_time - a.create_time)
    return parent
}

// 初始化子级
function initChild(parentItem, data) {
    let child = []
    for (let item of data) {
        if (parentItem.id == item.parent_id) {
            child.push(item)
        }
    }

    // 存在更多子级
    if (child.length > 0) {
        parentItem['more'] = true
        parentItem['total'] = child.length

    }

    // 递归调用
    // if (parentItem['children'] && parentItem['children'].length > 0) {
    //     for (let childItem of child) {
    //         initChild(childItem, data)
    //     }
    // }
}

module.exports = initTree