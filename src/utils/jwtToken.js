const { expressjwt } = require('express-jwt')
const { SECRET } = require('../config/config')


// 初始化expressJwt
const expressJwtToken = expressjwt({
    //秘钥，必须和jsonwebtoken保持一直
    secret: SECRET,

    // 加密方式
    algorithms: ['HS256'],

    // 获取token令牌
    async getToken(req) {
        if (req.headers.authorization) {
            return req.headers.authorization.split(' ')[1]
        }
    },

    // 是否开启拦截
    credentialsRequired: true
}).unless({
    // 白名单
    path: ['/api/login', '/api/register']
})

module.exports = expressJwtToken