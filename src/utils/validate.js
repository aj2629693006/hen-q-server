// 邮箱正则
const emailReg = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+(\.[a-zA-Z0-9]+)$/

// 注册信息校验
module.exports = {

    registerValidate(data) {
        const { nickname, email, password } = data
        let result = { state: false, message: '' }

        // 校验规则
        if (!nickname || nickname.length < 0) {
            result = { state: false, message: '昵称不能为空' }
        } else if (!email || !emailReg.test(email)) {
            result = { state: false, message: '邮箱格式错误' }
        } else if (!password || password.length < 0) {
            result = { state: false, message: '密码不能为空' }
        } else {
            result = { state: true, message: '', nickname, email, password }
        }

        // 返回值
        return result
    },

    // 登录信息校验
    loginValidate(data) {
        const { email, password } = data
        let result = { state: false, message: '' }

        // 校验规则
        if (!email || !emailReg.test(email)) {
            result = { state: false, message: '邮箱格式错误' }
        } else if (!password || password.length < 0) {
            result = { state: false, message: '密码不能为空' }
        } else {
            result = { state: true, message: '', email, password }
        }

        // 返回值
        return result
    }
}
