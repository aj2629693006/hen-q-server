const query = require('../service/db')
const jwt = require('jsonwebtoken')
const { SECRET } = require('../config/config')

// 获取当前账号相关数据
async function curAccount(authorization) {
    if (!authorization) return
    let token = authorization.split(' ')[1]
    return await jwt.verify(token, SECRET)
}

module.exports = async (req, res, next) => {

    if (req.auth && req.headers.authorization) {
        // 获取存在token的登录key
        const { key } = await curAccount(req.headers.authorization)
        console.log(req.auth);
        // 根据当前请求的账号ID查询数据库存储的登录key进行比对是否一致
        const sql = `select * from login_log where login_id=?`
        query(sql, [req.auth.account_id]).then(result => {

            // key对比    
            if (result.length > 0 && (key !== result[0].login_key)) {
                res.send({ code: 10008, msg: '你的账号已在其他设备登录，当前设备已下线' })
            } else {
                next()
            }
        }).catch(err => next(err));

    } else {
        next()
    }
}