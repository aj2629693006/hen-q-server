const express = require("express");
const expressSession = require("express-session")
const expressWs = require("express-ws")
const path = require('path')
const bodyParser = require("body-parser")
const router = require("./src/router");
const expressJwtToken = require("./src/utils/jwtToken")

// 实例化服务器
const app = express()

// 挂载WebSocket
expressWs(app)

// session配置
app.use(expressSession({
    // 唯一表示
    secret: 'key session',
    // 名称
    name: 'session',
    // 初始化session时是否保存到存储
    saveUninitialized: false,
    // 当前的值没有修改也需要对当前值进行保存
    resave: false,
    // 向服务发送请求后，是否重置cookie时间，建议true
    rolling: true,
    // cookie配置项
    cookie: {
        // 配置session过期时间
        maxAge: 1000 * 60 * 1
    }
}))


// POST请求获取参数方式
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


// 配置静态资源
app.use(express.static(path.join(__dirname, 'public')))


// 跨域处理
app.all("*", (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'POST,DELETE,PUT,GET,OPTIONS')
    res.header('Access-Control-Allow-Headers', 'Content-Type,authorization,token')
    next()
})


// WebSocket
const { newWebSocket } = require("./src/controller/wsController");
newWebSocket(app)

// 路由权限拦截 
app.use(expressJwtToken)

// 挂载接口
const permission = require("./src/utils/permission");
app.use('/api', permission, router)


//权限控制
app.use((err, req, res, next) => {
    switch (err.message) {
        case 'jwt expired':
            res.send({ code: 10003, msg: 'token已过期，请重新登录' })
            break
        default:
            res.send({ code: 10004, msg: '未授权访问' })
            break
    }
})

// 配置端口
app.listen(8016, () => {
    console.log('服务器启动成功');
})

